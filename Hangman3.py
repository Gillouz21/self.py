HANGMAN_ASCII_ART ="""Welcome to the game Hangman
  _    _                                         
 | |  | |                                        
 | |__| | __ _ _ __   __ _ _ __ ___   __ _ _ __  
 |  __  |/ _` | '_ \ / _` | '_ ` _ \ / _` | '_ \ 
 | |  | | (_| | | | | (_| | | | | | | (_| | | | |
 |_|  |_|\__,_|_| |_|\__, |_| |_| |_|\__,_|_| |_|
                      __/ |                      
                     |___/
"""

MAX_TRIES = 6

print(HANGMAN_ASCII_ART, MAX_TRIES)

letter = input("Guess a letter:")
print(letter.casefold())

#3.5.2
word = input("Please enter a word: ")
print("_ " * len(word))