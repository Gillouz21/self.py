def print_starting_screen():
    HANGMAN_ASCII_ART ="""Welcome to the game Hangman
  _    _                                         
 | |  | |                                        
 | |__| | __ _ _ __   __ _ _ __ ___   __ _ _ __  
 |  __  |/ _` | '_ \ / _` | '_ ` _ \ / _` | '_ \ 
 | |  | | (_| | | | | (_| | | | | | | (_| | | | |
 |_|  |_|\__,_|_| |_|\__, |_| |_| |_|\__,_|_| |_|
                      __/ |                      
                     |___/
    """
    MAX_TRIES = 6
    print(HANGMAN_ASCII_ART)
    print(MAX_TRIES)

def print_hangman(num_of_tries):
    HANGMAN_PHOTOS = {0 :"    x-------x",1 :
              """    x-------x
    |
    |
    |
    |
    |""",2 :
 """    x-------x
    |       |
    |       0
    |
    |
    |
    """,3 :
 """    x-------x
    |       |
    |       0
    |       |
    |
    |""",4 :
             """    x-------x
    |       |
    |       0
    |      /|\\
    |
    |""",5 :
              """    x-------x
    |       |
    |       0
    |      /|\\
    |      /
    |""",6 :
              """    x-------x
    |       |
    |       0
    |      /|\\
    |      / \\
    |"""}
    print(HANGMAN_PHOTOS[num_of_tries])

def choose_word(file_path, index):
    f = open(file_path, 'r')
    txt = f.read()

    arr = []
    for word in txt.split():
        if word not in arr:
            arr.append(word)

    return (len(arr), txt.split()[(index - 1) % len(txt.split())])

def show_hidden_word(secret_word, old_letters_guessed):
    ret_str = ""
    for char in secret_word:
        if char in old_letters_guessed:
            ret_str += (char + ' ')
        else:
            ret_str += ('_ ')
    return ret_str

def check_valid_input(letter_guessed, old_letters_guessed):
    flag = True
    letter_guessed = letter_guessed.casefold()
    if len(letter_guessed) > 1:
        flag = False
    elif not letter_guessed.isalpha():
        flag = False
    elif letter_guessed in old_letters_guessed:
        flag = False
    return flag

def try_update_letter_guessed(letter_guessed, old_letters_guessed):
    if check_valid_input(letter_guessed,old_letters_guessed):
        old_letters_guessed.append(letter_guessed.casefold())
        return True
    else:
        print("X")
        print(" -> ".join(sorted(old_letters_guessed)))
        return False

def check_win(secret_word, old_letters_guessed):
    for char in secret_word:
        if char not in old_letters_guessed:
            return False
    return True





# ______________________________________ #
MAX_TRIES = 6
current_turn = 0
old_letters_guessed = []
print_starting_screen()
file_path = "C:\\Users\\User\Desktop\Army\selfPy\self.py\source.txt"#input("Enter file path: ")
index = 15 #int(input("enter index: "))
_, secret_word = choose_word(file_path, index)

print("Let’s start!\n")
print_hangman(current_turn)
print(show_hidden_word(secret_word, old_letters_guessed))


while current_turn < MAX_TRIES and not check_win(secret_word, old_letters_guessed):
    letter_guessed = input("enter a char: ")
    if try_update_letter_guessed(letter_guessed, old_letters_guessed):
        if letter_guessed.casefold() in secret_word:
            print(show_hidden_word(secret_word,old_letters_guessed))
        else:
            print("):")
            current_turn +=1
            print_hangman(current_turn)
            print(show_hidden_word(secret_word, old_letters_guessed))

if check_win(secret_word, old_letters_guessed):
    end = "WIN"
else:
    end = "LOSE"
print(end)
