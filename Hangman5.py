def is_valid_input(letter_guessed):
    flag = True
    if len(letter_guessed) > 1:
        flag = False
    elif not letter_guessed.isalpha():
        flag = False
    return flag


print(is_valid_input('a'))
print(is_valid_input('A'))
print(is_valid_input('$'))
print(is_valid_input('ab'))
print(is_valid_input('app$'))
