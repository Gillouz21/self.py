#6.4.1
def check_valid_input(letter_guessed, old_letters_guessed):
    flag = True
    letter_guessed = letter_guessed.casefold()
    if len(letter_guessed) > 1:
        flag = False
    elif not letter_guessed.isalpha():
        flag = False
    elif letter_guessed in old_letters_guessed:
        flag = False
    print (flag)


old_letters = ['a', 'b', 'c']
check_valid_input('C', old_letters)
check_valid_input('ep', old_letters)
check_valid_input('$', old_letters)
check_valid_input('s', old_letters)


#6.4.2
def try_update_letter_guessed(letter_guessed, old_letters_guessed):
    if check_valid_input(letter_guessed,old_letters_guessed):
        old_letters_guessed.append(letter_guessed.casefold())
        return True
    else:
        print("X")
        print(" -> ".join(sorted(old_letters_guessed)))
        return False


old_letters = ['a', 'p', 'c', 'f']
print(try_update_letter_guessed('A', old_letters))
print(try_update_letter_guessed('s', old_letters))
print(old_letters)
print(try_update_letter_guessed('$', old_letters))
print(try_update_letter_guessed('d', old_letters))
print(old_letters)