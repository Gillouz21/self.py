def choose_word(file_path, index):
    f = open(file_path, 'r')
    txt = f.read()

    arr = []
    for word in txt.split():
        if word not in arr:
            arr.append(word)

    return (len(arr), txt.split()[(index - 1) % len(txt.split())])


print(choose_word("C:\\Users\\User\Desktop\Army\selfPy\self.py\source.txt", 3))
print(choose_word("C:\\Users\\User\Desktop\Army\selfPy\self.py\source.txt", 15))