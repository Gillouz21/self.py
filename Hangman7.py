#7.3.1
def show_hidden_word(secret_word, old_letters_guessed):
    ret_str = ""
    for char in secret_word:
        if char in old_letters_guessed:
            ret_str += (char + ' ')
        else:
            ret_str += ('_ ')
    return ret_str

secret_word = "mammals"
old_letters_guessed = ['s', 'p', 'j', 'i', 'm', 'k']
print(show_hidden_word(secret_word, old_letters_guessed))


#7.3.2
def check_win(secret_word, old_letters_guessed):
    for char in secret_word:
        if char not in old_letters_guessed:
            return False
    return True


secret_word = "friends"
old_letters_guessed = ['m', 'p', 'j', 'i', 's', 'k']
print(check_win(secret_word, old_letters_guessed))

secret_word = "yes"
old_letters_guessed = ['d', 'g', 'e', 'i', 's', 'k', 'y']
print(check_win(secret_word, old_letters_guessed))