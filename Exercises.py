#Unit 2
#2.3.3
a = input("enter three digits (one for each pig)")
sum = int(a[0]) + int(a[1]) + int(a[2])
even_distributed_bricks = sum //3
bricks_left = sum % 3
is_evenly_distributed = sum % 3 == 0
print(sum, even_distributed_bricks,bricks_left, is_evenly_distributed)

#Unit 3
#3.2.1
print("""\"Shuffle, Shuffle, Shuffle\", say it together! \nChange colors and directions, \nDon't back down and stop the player! \n\tDo you want to play Taki? \n\tPress y\\n""")

#3.3.3
encrypted_message = "!XgXnXiXcXiXlXsX XnXoXhXtXyXpX XgXnXiXnXrXaXeXlX XmXaX XI"
print(encrypted_message[::-2])

#3.4.2
sentence = input("Please enter a string: ")
print(sentence[0] + sentence[1:].replace(sentence[0],"e"))

#3.4.3
string = input("Please enter a string: ")
print(string[:(len(string)//2)].casefold() + string[(len(string)//2):].upper())

#Unit 4
#4.2.2
a = input("Enter a word: ").casefold().replace(" ","")
if(a == a[::-1]):
    print("OK")
else:
    print("NOT")

#4.2.3
degrees = input("Insert the temperature you would like to convert: ").casefold()
num = int(degrees[:len(degrees) - 1])
if degrees.endswith("f"):
    print(str((5 * num - 160) // 9) + "C")
else:
    print(str((9 * num + 160) // 5) + "F")

#4.2.4
import calendar
day, month, year = map(int, input("Enter a date: ").split("/"))
print(calendar.day_name[calendar.weekday(year, month, day)])

#5.3.4
def last_early(my_str):
    new_str = str.casefold(my_str)
    print(new_str.find(new_str[-1]) != len(new_str)-1)

last_early("X")

#5.3.5
def distance(num1, num2, num3):
    print((abs(num1-num2) == 1 or abs(num1 - num3) == 1) and (abs(num2 - num3) >= 2 and abs(num2 - num1) >= 2) or (abs(num2 - num3) >= 2 and abs(num3 - num1) >= 2))

distance(1,2,10)
distance(4,5,3)

#5.3.6
def filter_teens(a = 13, b = 13, c = 13):
    a = fix_age(a)
    b = fix_age(b)
    c = fix_age(c)
    print(a + b + c)

def fix_age(age):
    if (12 < age < 15 or 16 < age < 19):
        return 0
    else:
        return age


filter_teens(2, 13, 1)
filter_teens(2, 1, 15)

#5.3.7
def chocolate_maker(small, big, x):
    big_flag = False
    small_flag = False
    if (x == 0):
        return True

    if(big > 0):
        big_flag = chocolate_maker(small, big - 1, x - 5)
    if(small > 0):
        small_flag = chocolate_maker(small -1, big, x - 1)

    return big_flag or small_flag

print(chocolate_maker(3, 1, 8))
print(chocolate_maker(3, 1, 9))
print(chocolate_maker(3, 2, 10))

#5.4.1

def func(num1, num2):
    """Calculates a number raised to a power.
     :param num1: base value
     :param num2: exponent value
     :type num1: int
     :type num2: int
     :return: The result of raising num1 to the power num2
     :rtype: int
     """
    return num1 ** num2
def main():
    print(func(4,2))

if __name__ == "__main__":
    main()



#unit6
#6.1.2
def shift_left(my_list):
    return [my_list[1], my_list[2], my_list[0]]

#6.2.3
def format_list(my_list):
    return ", ".join(my_list[::2]) + " and " + my_list[-1]


my_list = ["hydrogen", "helium", "lithium", "beryllium", "boron", "magnesium"]
print(format_list(my_list))
print(shift_left([0, 1, 2]))
print(shift_left(['monkey', 2.0, 1]))

#6.2.4
def extend_list_x(list_x, list_y):
    original_len_x = len(list_x)
    list_x[original_len_x::] = list_x
    list_x[0:original_len_x] = list_y
    return list_x


x = [4, 5, 6]
y = [1, 2, 3]
print(extend_list_x(x, y))

#6.3.1

def are_lists_equal(list1, list2):
    return sorted(list1) == sorted(list2)


list1 = [0.6, 1, 2, 3]
list2 = [3, 2, 0.6, 1]
list3 = [9, 0, 5, 10.5]

print(are_lists_equal(list1, list2))
print(are_lists_equal(list1, list3))

#6.3.2
def longest(my_list):
    return max(my_list, key=len)


list1 = ["111", "234", "2000", "goru", "birthday", "09"]
print(longest(list1))

#unit7
#7.1.4
def squared_numbers(start, stop):
    squared_list = []
    while start <= stop:
        squared_list.append(start ** 2)
        start += 1
    return squared_list


print(squared_numbers(4, 8))
print(squared_numbers(-3, 3))


#7.2.1
def is_greater(my_list, n):
    new_list= []
    for num in my_list:
        if num > n:
            new_list.append(num)
    return new_list


result = is_greater([1, 30, 25, 60, 27, 28], 28)
print(result)



#7.2.2
def numbers_letters_count(my_str):
    letters = 0
    numbers = 0
    for char in my_str:
        if char.isnumeric():
            numbers += 1
        else:
            letters += 1

    return [numbers,letters]


print(numbers_letters_count("Python 3.6.3"))

#7.2.4
def seven_boom(end_number):
    arr = []
    for num in range(end_number + 1):
        if num % 10 == 7 or num % 7 == 0:
            arr.append('BOOM')
        else:
            arr.append(num)
    return arr


print(seven_boom(17))

#7.2.5
def sequence_del(my_str):
    new_str = ""
    prev_char = ""
    for char in my_str:
        if char != prev_char:
            new_str += char
        prev_char = char
    return new_str

print(sequence_del("ppyyyyythhhhhooonnnnn"))
print(sequence_del("SSSSsssshhhh"))
print(sequence_del("Heeyyy   yyouuuu!!!"))

#7.2.6
def groceries_func(my_str):
    while True:
        num = int(input("insert a number between 1 to 9"))
        if num == 1:
            print(my_str)
        if num == 2:
            print(len(my_str))
        if num == 3:
            is_in_list = input("enter product name")
            if is_in_list in my_str:
                print(True)
            else:
                print(False)
        if num == 4:
            times_in_list = input("enter product name")
            counter = 0
            for product in my_str:
                if product == times_in_list:
                    counter += 1
            print(counter)
        if num == 5:
            product = input("enter product name")
            my_str.remove(product)
        if num == 6:
            product = input("enter product name")
            my_str.append(product)
        if num == 7:
            for product in my_str:
                if len(product) < 3 or not product.isalpha():
                    print(product)
        if num == 8:
            new_list = []
            for product in my_str:
                if product not in new_list:
                    new_list.append(product)
            my_str = new_list
        if num == 9:
            break


groceries_func(["Milk", "Cottage", "Tomatoes"])


#7.2.7
def arrow(my_char, max_length):
    arrow = ""
    for i in range(1, max_length + 1):
        arrow += (my_char + ' ') * i + "\n"
    for i in range(max_length - 1, 0, -1):
        arrow += (my_char + ' ') * i + "\n"
    return arrow


print(arrow('*', 5))


#Unit 8
#8.2.1
data = ("self", "py", 1.543)
format_string = "Hello %s.%s learner, you have only %.1f units left before you master the course!"

print(format_string % data)

#8.2.2
def MyFn(s):
    return s[-1]


def sort_prices(list_of_tuples):
    return sorted(list_of_tuples, key=MyFn, reverse=True)

products = [('milk', '5.5'), ('candy', '2.5'), ('bread', '9.0')]
print(sort_prices(products))

#8.2.3
def mult_tuple(tuple1, tuple2):
    result = []
    for num1 in tuple1:
        for num2 in tuple2:
            result.append((num1,num2))
            result.append((num2,num1))
    return tuple(result)


first_tuple = (1, 2, 3)
second_tuple = (4, 5, 6)
print(mult_tuple(first_tuple, second_tuple))

#8.2.4
def sort_anagrams(list_of_strings):
    anag_list = []
    temp_list = []
    while len(list_of_strings) > 0:
        temp = sorted(list_of_strings[0])
        for word in list_of_strings:
            if temp == sorted(word):
                temp_list.append(word)
        anag_list.append(temp_list.copy())
        for val in temp_list:
            list_of_strings.remove(val)
        temp_list.clear()
    return anag_list


list_of_words = ['deltas', 'retainers', 'desalt', 'pants', 'slated', 'generating', 'ternaries', 'smelters', 'termless', 'salted', 'staled', 'greatening', 'lasted', 'resmelts']
print(sort_anagrams(list_of_words))

#8.3.2
new_dict = {"first_name": "Mariah", "last_name" : "Carey", "birth_date" : "27.03.1970 ","hobbies" : ["Sing", "Compose", "Act"] }
x = int(input("number between 1 - 7"))
if x == 1:
    print(new_dict["last_name"])
if x == 2:
    print(new_dict["birth_date"][3:5])
if x == 3:
    print(len(new_dict["hobbies"]))
if x == 4:
    print(new_dict["hobbies"][-1])
if x == 5:
    new_dict["hobbies"].append("Cooking")
if x == 6:
    new_dict["birth_date"] = (27,3,1970)
    print(new_dict["birth_date"])
if x == 7:
    new_dict["age"] = 53

#8.3.3
def count_chars(my_str):
    new_dict = {}
    for char in my_str:
        if not char.isspace():
            if char not in new_dict:
                new_dict[char] = 1
            else:
                new_dict[char] += 1
    return new_dict


magic_str = "abra cadabra"
print(count_chars(magic_str))


#8.3.4
def inverse_dict(my_dict):
    new_dict = {}
    for value, key in my_dict.items():
        if key in new_dict:
            new_dict[key].append(value)
        else:
            new_dict[key] = [value]
    return new_dict


course_dict = {'I': 3, 'love': 3, 'self.py!': 2}
print(inverse_dict(course_dict))

#unit 9
#9.2.2
def copy_file_content(source, destination):
    source_file = open(source, 'r')
    buffer = source_file.read()
    source_file.close()

    destination_file = open(destination, 'w')
    destination_file.write(buffer)
    destination_file.close()

#9.2.3
def who_is_missing(file_name):
    fileos = open(file_name, 'r')
    buffer = fileos.read()
    buffer = buffer.rstrip('\n')
    read_list = buffer.split(',')
    read_list.sort()
    i = 1
    for num in read_list:
        if int(num) != i:
            missing = i
            break
        i += 1
    fileos.close()

    fileos = open("new_file", 'w')
    fileos.write(str(missing))
    fileos.close()
    return missing


print(who_is_missing("C:\\Users\\User\Desktop\Army\selfPy\self.py\source.txt"))

#9.3.1
def turn_to_sec(my_str):
    temp = my_str.split(':')
    return int(temp[0]) * 60 + int(temp[1])


def my_mp3_playlist(file_path):
    longest_name = ""
    longest_time = 0
    num_of_songs = 0
    common_singer = ""
    new_dict = {}
    fileos = open(file_path,'r')
    for row in fileos:
        arr = row.split(';')
        if turn_to_sec(arr[2]) > longest_time:
            longest_name = arr[0]
            longest_time = turn_to_sec(arr[2])
        if arr[1] not in new_dict:
            new_dict[arr[1]] = 1
        else:
            new_dict[arr[1]] += 1
        num_of_songs += 1
    return (longest_name, num_of_songs, max(new_dict, key=lambda k: new_dict[k]))


print(my_mp3_playlist("C:\\Users\\User\Desktop\Army\selfPy\self.py\source.txt"))

#9.3.2
def my_mp4_playlist(file_path, new_song):
    i = 0
    fileos = open(file_path, 'r')
    contents = fileos.readlines()
    while len(contents) < 2:
        contents.append("\n")

    if len(contents) < 3:
        contents.append(new_song)
    else:
        arr = contents[2].split(';')
        arr[0] = new_song
        line = ';'.join(arr)
        contents[2] = line

    fileos = open(file_path, "w")
    fileos.writelines(contents)

    fileos = open(file_path, 'r')
    for line in fileos:
        print(line)



my_mp4_playlist("C:\\Users\\User\Desktop\Army\selfPy\self.py\source.txt", "Python Love Story")


